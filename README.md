# Öppna upp :headphones:

Podcast about open source from a government.

[**Suggest a topic >>**](https://gitlab.com/arbetsformedlingen/www/podcast/-/issues/new)

<hr/>

[<img src="img/cover-min.png" width="650"/>](img/cover-min.png)

## Episodes

**Introduktion**

Tre programmerare resonerar om behovet av öppen källkod inom det offentliga.

![](public/avsnitt/2022-09-12_E0_introduktion.mp3)

**Öppen dataserver**

Fördelar med att dela data på ett enkelt sätt. Diskussion om https://data.arbetsformedlingen.se

[<img src="img/dataserver-skiss.png" width="450"/>](img/dataserver-skiss.png)

![](public/avsnitt/2022-09-22_E1_dataserver.mp3)

## Subscribe

https://arbetsformedlingen.gitlab.io/www/podcast/feed.xml

## Hosts

| Host   |     |
| --- | --- |
| @sodjs | ![](https://gitlab.com/uploads/-/system/user/avatar/1352400/avatar.png?width=400) |
| @jave | ![](https://secure.gravatar.com/avatar/334a47844c5042c716905758cdcc11c9?s=192&d=identicon?width=800) |
| @weipe | ![](https://secure.gravatar.com/avatar/68f44ad626a4d1edf99444bfe5750a01?s=192&d=identicon) |

## JINGLE
Feel free to study, modify, remix and share the jingle 
[here](https://gitlab.com/perweij/jingle-for-the-podcast-oeppna-upp).

## Acknowledgement

https://www.audacityteam.org/

https://obsproject.com/
